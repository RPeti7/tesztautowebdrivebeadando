﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace WebdriverClass.Beadando
{
    [TestFixture]
    public class BeadandoTest : TestBase
    {
        [SetUp]
        public void Setup()
        {
            Driver.Navigate().GoToUrl("https://www.surveymonkey.com/r/FNRVK86");
        }

        [Test]
        public void NavigateToSurveysUrl_ShouldHaveRightTitle()
        {
            // Assert
            Assert.That(Driver.Title, Is.EqualTo("Obuda2019OszBeadando Survey"));
        }

        [Test]
        public void InFirstQuestionThirdLabel_ShouldBeSelected()
        {
            // Act
            Driver.FindElement(By.CssSelector("#question-field-373484282 > fieldset > div > div > div:nth-child(3)")).Click();

            // Assert
            Assert.That(Driver.FindElement(By.Id("373484282_2479075470")).Selected, Is.True);
        }

        [Test]
        public void InSecondQuestionThirdLabel_ShouldBeSelected()
        {
            // Act
            Driver.FindElement(By.CssSelector("#question-field-373480132 > fieldset > div > div > div:nth-child(3)")).Click();

            // Assert
            Assert.That(Driver.FindElement(By.Id("373480132_2479050980")).Selected, Is.True);
        }

        [Test]
        public void InThirdQuestionSecondElement_ShouldBeSelected()
        {
            // Act
            new SelectElement(Driver.FindElement(By.CssSelector("#\\33 73480133"))).SelectByIndex(2);

            // Assert
            Assert.That(Driver.FindElement(By.Id("373480133")).Text.Contains("Webdriver advanced"));
        }

        [Test]
        public void InFourthQuestionFirstSelectionFirstElement_ShouldBeSelected()
        {
            // Act
            new SelectElement(Driver.FindElement(By.CssSelector("#\\33 73480134_2479050988"))).SelectByIndex(1);

            // Assert
            Assert.That(Driver.FindElement(By.Id("373480134_2479050988")).Text.Contains("1"));
        }

        [Test]
        public void InFourthQuestionSecondSelectionSecondElement_ShouldBeSelected()
        {
            // Act
            new SelectElement(Driver.FindElement(By.CssSelector("#\\33 73480134_2479050997"))).SelectByIndex(2);

            // Assert
            Assert.That(Driver.FindElement(By.Id("373480134_2479050997")).Text.Contains("2"));
        }

        [Test]
        public void InFourthQuestionThirdSelectionFifthElement_ShouldBeSelected()
        { 
            // Act
            new SelectElement(Driver.FindElement(By.CssSelector("#\\33 73480134_2479050998"))).SelectByIndex(5);

            // Assert
            Assert.That(Driver.FindElement(By.Id("373480134_2479050998")).Text.Contains("5"));
        }

        [Test]
        public void InFourthQuestionFourthSelectionThirdElement_ShouldBeSelected()
        {
            // Act
            new SelectElement(Driver.FindElement(By.CssSelector("#\\33 73480134_2479050999"))).SelectByIndex(3);

            // Assert
            Assert.That(Driver.FindElement(By.Id("373480134_2479050999")).Text.Contains("3"));
        }

        [Test]
        public void InFourthQuestionFifthSelectionFourthElement_ShouldBeSelected()
        {
            // Act
            new SelectElement(Driver.FindElement(By.CssSelector("#\\33 73480134_2479051000"))).SelectByIndex(4);

            // Assert
            Assert.That(Driver.FindElement(By.Id("373480134_2479051000")).Text.Contains("4"));
        }

        [Test]
        public void NextButtonClick_ShouldNavigateToNextPage()
        {
            // Arrange
            Driver.FindElement(By.CssSelector("#question-field-373484282 > fieldset > div > div > div:nth-child(3)")).Click();
            Driver.FindElement(By.CssSelector("#question-field-373480132 > fieldset > div > div > div:nth-child(3)")).Click();
            new SelectElement(Driver.FindElement(By.CssSelector("#\\33 73480133"))).SelectByIndex(2);
            new SelectElement(Driver.FindElement(By.CssSelector("#\\33 73480134_2479050988"))).SelectByIndex(1);
            new SelectElement(Driver.FindElement(By.CssSelector("#\\33 73480134_2479050997"))).SelectByIndex(2);
            new SelectElement(Driver.FindElement(By.CssSelector("#\\33 73480134_2479050998"))).SelectByIndex(5);
            new SelectElement(Driver.FindElement(By.CssSelector("#\\33 73480134_2479050999"))).SelectByIndex(3);
            new SelectElement(Driver.FindElement(By.CssSelector("#\\33 73480134_2479051000"))).SelectByIndex(4);

            // Act
            Driver.FindElement(By.CssSelector("#patas > main > article > section > form > div.survey-submit-actions.center-text.clearfix > button")).Click();

            // Assert
            Assert.That(Driver.FindElement(By.CssSelector("#question-title-373480135 > span.user-generated.notranslate")).Displayed);
        }

        [Test]
        public void InFifthQuestionThirdLabel_ShouldBeSelected()
        {
            // Arrange
            Driver.FindElement(By.CssSelector("#question-field-373484282 > fieldset > div > div > div:nth-child(3)")).Click();
            Driver.FindElement(By.CssSelector("#question-field-373480132 > fieldset > div > div > div:nth-child(3)")).Click();
            new SelectElement(Driver.FindElement(By.CssSelector("#\\33 73480133"))).SelectByIndex(2);
            new SelectElement(Driver.FindElement(By.CssSelector("#\\33 73480134_2479050988"))).SelectByIndex(1);
            new SelectElement(Driver.FindElement(By.CssSelector("#\\33 73480134_2479050997"))).SelectByIndex(2);
            new SelectElement(Driver.FindElement(By.CssSelector("#\\33 73480134_2479050998"))).SelectByIndex(5);
            new SelectElement(Driver.FindElement(By.CssSelector("#\\33 73480134_2479050999"))).SelectByIndex(3);
            new SelectElement(Driver.FindElement(By.CssSelector("#\\33 73480134_2479051000"))).SelectByIndex(4);
            Driver.FindElement(By.CssSelector("#patas > main > article > section > form > div.survey-submit-actions.center-text.clearfix > button")).Click();

            // Act
            Driver.FindElement(By.CssSelector("#question-field-373480135 > fieldset > div > div > div:nth-child(3) > div > label")).Click();

            // Assert
            Assert.That(Driver.FindElement(By.Id("373480135_2479050991")).Selected, Is.True);
        }

        [Test]
        public void InSixthQuestionTextBox_ShouldContainGivenText()
        {
            // Arrange
            Driver.FindElement(By.CssSelector("#question-field-373484282 > fieldset > div > div > div:nth-child(3)")).Click();
            Driver.FindElement(By.CssSelector("#question-field-373480132 > fieldset > div > div > div:nth-child(3)")).Click();
            new SelectElement(Driver.FindElement(By.CssSelector("#\\33 73480133"))).SelectByIndex(2);
            new SelectElement(Driver.FindElement(By.CssSelector("#\\33 73480134_2479050988"))).SelectByIndex(1);
            new SelectElement(Driver.FindElement(By.CssSelector("#\\33 73480134_2479050997"))).SelectByIndex(2);
            new SelectElement(Driver.FindElement(By.CssSelector("#\\33 73480134_2479050998"))).SelectByIndex(5);
            new SelectElement(Driver.FindElement(By.CssSelector("#\\33 73480134_2479050999"))).SelectByIndex(3);
            new SelectElement(Driver.FindElement(By.CssSelector("#\\33 73480134_2479051000"))).SelectByIndex(4);
            Driver.FindElement(By.CssSelector("#patas > main > article > section > form > div.survey-submit-actions.center-text.clearfix > button")).Click();
            Driver.FindElement(By.CssSelector("#question-field-373480135 > fieldset > div > div > div:nth-child(3) > div > label")).Click();

            // Act
            Driver.FindElement(By.CssSelector("#\\33 73480136")).SendKeys("Nincs! Köszönjük a tárgyat!");

            // Assert
            Assert.That(Driver.FindElement(By.Id("373480136")).GetAttribute("value").Contains("Nincs! Köszönjük a tárgyat!"), Is.True);
        }

        [Test]
        public void AfterDoneButtonPressTitle_ShouldEqualToGiven()
        {
            // Arrange
            Driver.FindElement(By.CssSelector("#question-field-373484282 > fieldset > div > div > div:nth-child(3)")).Click();
            Driver.FindElement(By.CssSelector("#question-field-373480132 > fieldset > div > div > div:nth-child(3)")).Click();
            new SelectElement(Driver.FindElement(By.CssSelector("#\\33 73480133"))).SelectByIndex(2);
            new SelectElement(Driver.FindElement(By.CssSelector("#\\33 73480134_2479050988"))).SelectByIndex(1);
            new SelectElement(Driver.FindElement(By.CssSelector("#\\33 73480134_2479050997"))).SelectByIndex(2);
            new SelectElement(Driver.FindElement(By.CssSelector("#\\33 73480134_2479050998"))).SelectByIndex(5);
            new SelectElement(Driver.FindElement(By.CssSelector("#\\33 73480134_2479050999"))).SelectByIndex(3);
            new SelectElement(Driver.FindElement(By.CssSelector("#\\33 73480134_2479051000"))).SelectByIndex(4);
            Driver.FindElement(By.CssSelector("#patas > main > article > section > form > div.survey-submit-actions.center-text.clearfix > button")).Click();
            Driver.FindElement(By.CssSelector("#question-field-373480135 > fieldset > div > div > div:nth-child(3) > div > label")).Click();
            Driver.FindElement(By.CssSelector("#\\33 73480136")).SendKeys("Nincs! Köszönjük a tárgyat!");

            // Act
            Driver.FindElement(By.CssSelector("#patas > main > article > section > form > div.survey-submit-actions.center-text.clearfix > button.btn.small.done-button.survey-page-button.user-generated.notranslate")).Click();

            // Assert
            Assert.That(Driver.Title.Contains("Thank you! Create Your Own Online Survey Now!"));
        }
    }
}
