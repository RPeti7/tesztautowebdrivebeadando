﻿using OpenQA.Selenium;

namespace WebdriverClass.Pages
{
    class BasePage
    {
        protected IWebDriver Driver;

        public BasePage(IWebDriver webDriver)
        {
            Driver = webDriver;
        }
    }
}
