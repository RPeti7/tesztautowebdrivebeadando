﻿using System;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;

namespace WebdriverClass.Tests
{
    class CookieTestAtClass : TestBase
    {
        String cookieName = "visitorid";

        [Test]
        public void TestVisitorCookie()
        {
            //Delete all cookies
            Driver.Navigate().GoToUrl("http://bookline.hu/");

            // Get visitorid cookie from bookline page
            Cookie cookieUnderTest = null;
            Assert.IsNotNull(cookieUnderTest);
            PrintCookieInfo(cookieUnderTest);

            SearchForBook("Szent Johanna Gimi");
            // Get visitorid cookie again from bookline page
            Cookie updatedCookie = null;
            Assert.IsNotNull(updatedCookie);

            // verify that the VALUE of the 'visitorid' cookie is still the same
            Assert.AreEqual(1, 2);

            // verify that the EXPIRATION DATE of the 'visitorid' cookie is still the same
            Assert.AreEqual(1, 2);
        }

        [Test]
        public void TestCookieDeletion()
        {
            Driver.Navigate().GoToUrl("http://bookline.hu/");

            // Get visitorid cookie from bookline page
            Cookie cookieUnderTest = null;
            Assert.IsNotNull(cookieUnderTest);
            PrintCookieInfo(cookieUnderTest);

            // Save cookieundertest value
            String originalValue = "";
            Assert.IsTrue(originalValue.Length > 0);

            // Delete visitorid cookie
            // Verifiy that visitorid cookie is missing by getting a null
            Assert.IsNull("not null");

            SearchForBook("Szent Johanna Gimi");
            // Get visitorid cookie from bookline page again
            Cookie recreatedCookie = null;
            PrintCookieInfo(recreatedCookie);

            // verify that the original and recreated cookie value is different
            Assert.AreNotEqual(1, 1);
        }

        private void PrintCookieInfo(Cookie testedCookie)
        {
            Console.Write("\n*******************************************************************\n");
            Console.Write("NAME: " + testedCookie.Name +
                    "\nVALUE: " + testedCookie.Value +
                    "\nEXPIRY: " + testedCookie.Expiry +
                    "\nDOMAIN " + testedCookie.Domain +
                    "\nPATH " + testedCookie.Path +
                    "\nIS SECURE: " + testedCookie.Secure);
            Console.Write("\n*******************************************************************\n");
        }

        private void SearchForBook(String bookName)
        {
            var search_field = Driver.FindElement(By.Name("searchfield"));
            search_field.SendKeys(bookName);
            search_field.Submit();
            Thread.Sleep(200);
        }
    }
}
