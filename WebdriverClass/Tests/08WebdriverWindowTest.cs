﻿using System;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;

namespace WebdriverClass.Tests
{
    class WebdriverWindowTest : TestBase
    {
        [Test]
        public void ResponsiveWindow()
        {
            Driver.Navigate().GoToUrl("http://www.expedia.com/");
            Driver.Manage().Window.Maximize();
            Assert.IsTrue(Driver.FindElement(By.Id("header-account-menu")).Displayed);
            Assert.IsFalse(Driver.FindElement(By.Id("header-mobile-toggle")).Displayed);
            Driver.Manage().Window.Size = new Size(600, 600);
            Assert.IsTrue(Driver.FindElement(By.Id("header-mobile-toggle")).Displayed);
            Assert.IsFalse(Driver.FindElement(By.Id("header-account-menu")).Displayed);
        }

        [Test]
        public void MultipleWindow()
        {
            Driver = new ChromeDriver();
            Driver.Navigate().GoToUrl("https://www.amazon.com/gp/gw/ajax/s.html");
            String firstWindow = Driver.CurrentWindowHandle;

            new Actions(Driver).KeyDown(Keys.Shift).Click(Driver.FindElement(By.CssSelector("a[href*='cart']"))).KeyUp(Keys.Shift).Perform();
            ReadOnlyCollection<string> windows = Driver.WindowHandles;
            Driver.SwitchTo().Window(windows.Last());
            StringAssert.Contains("Cart", Driver.Title);
            Driver.Close();
            Driver.SwitchTo().Window(firstWindow);
            Assert.IsTrue(Driver.FindElement(By.Id("gw-card-layout")).Displayed);
        }
    }
}
