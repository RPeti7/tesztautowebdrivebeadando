﻿using System;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;

namespace WebdriverClass.Tests
{
    class CookieTest : TestBase
    {
        String visitorCookie = "visitorid";

        [Test]
	    public void TestVisitorCookie()
        {
		    Driver.Manage().Cookies.DeleteAllCookies();
		    Driver.Navigate().GoToUrl("http://bookline.hu/");

            Cookie cookieUnderTest = Driver.Manage().Cookies.GetCookieNamed(visitorCookie);
		    // verify that the test cookie exists
            Assert.IsNotNull(cookieUnderTest);
		    PrintCookieInfo(cookieUnderTest);
		
		    SearchForBook("Szent Johanna Gimi");
            Cookie updatedCookie = Driver.Manage().Cookies.GetCookieNamed(visitorCookie);
		
		    // verify that the VALUE of the 'visitorid' cookie is still the same
            Assert.AreEqual(cookieUnderTest.Value, updatedCookie.Value);
		
		    // verify that the EXPIRATION DATE of the 'visitorid' cookie is still the same
            Assert.AreEqual(cookieUnderTest.Expiry, updatedCookie.Expiry);
	    }

        [Test]
	    public void TestCookieDeletion()
        {
		    Driver.Navigate().GoToUrl("http://bookline.hu/");
		    Cookie cookieUnderTest = Driver.Manage().Cookies.GetCookieNamed(visitorCookie);
		    // verify that the test cookie exists
            Assert.IsNotNull(cookieUnderTest);
            PrintCookieInfo(cookieUnderTest);

		    String originalValue = cookieUnderTest.Value;
		    Assert.IsTrue(originalValue.Length > 0);
		    		
		    Driver.Manage().Cookies.DeleteCookieNamed(visitorCookie);
            Assert.IsNull(Driver.Manage().Cookies.GetCookieNamed(visitorCookie));	
		
		    SearchForBook("Szent Johanna Gimi");
            Cookie recreatedCookie = Driver.Manage().Cookies.GetCookieNamed(visitorCookie);
            PrintCookieInfo(recreatedCookie);

            // verify that the original and recreated cookie value is different
            Assert.AreNotEqual(originalValue, recreatedCookie.Value);
	    }

        private void PrintCookieInfo(Cookie testedCookie)
        {
            Console.Write("\n*******************************************************************\n");
            Console.Write("NAME: " + testedCookie.Name +
                    "\nVALUE: " + testedCookie.Value +
                    "\nEXPIRY: " + testedCookie.Expiry +
                    "\nDOMAIN " + testedCookie.Domain +
                    "\nPATH " + testedCookie.Path +
                    "\nIS SECURE: " + testedCookie.Secure);
            Console.Write("\n*******************************************************************\n");
        }

        private void SearchForBook(String bookName)
        {
            var search_field = Driver.FindElement(By.Name("searchfield"));
            search_field.SendKeys(bookName);
            search_field.Submit();
            Thread.Sleep(200);
        }
    }
}
