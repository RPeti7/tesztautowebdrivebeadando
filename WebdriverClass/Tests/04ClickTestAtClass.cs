﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace WebdriverClass.Tests
{
    class ClickTestAtClass : TestBase
    {
        [Test]
        public void ClickExample()
        {
            Driver.Navigate().GoToUrl("https://stackoverflow.com/questions");
            //Assert that "Newest Questions - Stack Overflow" is the title

            //Click on Tags link (use Id selector)
            Assert.AreEqual("Tags - Stack Overflow", Driver.Title);

            //Click on Stackoverflow logo link (use CssSelector selector)
            StringAssert.Contains("Where Developers Learn", Driver.Title);
        }

        [Test]
        public void ShiftClickExample()
        {
            Driver.Navigate().GoToUrl("https://www.amazon.com/gp/gw/ajax/s.html");
            //Use Actions class to create a shift click on Cart link
            //You have to see two browser windows after a successful run
            Assert.AreEqual(2, Driver.WindowHandles.Count);
        }

        [Test]
        public void DragAndDropTestAtClass()
        {
            Driver.Navigate().GoToUrl("https://jqueryui.com/resources/demos/droppable/default.html");
            //Locate the draggable element
            //Locate the droppable element
            //Add the drag and drop action here
            Assert.IsTrue(Driver.FindElement(By.CssSelector("#droppable > p:nth-child(1)")).Text.Equals("Dropped!"));
        }
    }
}
