﻿using System;
using NUnit.Framework;

namespace WebdriverClass.Tests
{
    class NavigationTestAtClass : TestBase
    {
        private String assertTitleChange(String oldTitle)
        {
            String newTitle = Driver.Title;
            Assert.AreNotEqual(oldTitle, newTitle);
            return newTitle;
        }

        [Test]
        public void NavigationExample()
        {
            String title = "";
            
            //Navigate to https://www.google.com
            title = assertTitleChange(title);
            
            //Navigate to https://www.bing.com
            title = assertTitleChange(title);
            
            //Navigate back
            title = assertTitleChange(title);
            
            //Navigate forward
            title = assertTitleChange(title);
            
            //Refresh browser window
            Assert.AreEqual(title, Driver.Title);
            Assert.AreEqual("https://www.bing.com/", Driver.Url);
        }
    }
}
