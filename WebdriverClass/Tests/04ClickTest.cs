﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace WebdriverClass.Tests
{
    class ClickTest : TestBase
    {
        [Test]
        public void ClickExample()
        {
            Driver.Navigate().GoToUrl("https://stackoverflow.com/questions");
            Assert.AreEqual("Newest Questions - Stack Overflow", Driver.Title);

            Driver.FindElement(By.Id("nav-tags")).Click();
            Assert.AreEqual("Tags - Stack Overflow", Driver.Title);

            Driver.FindElement(By.CssSelector("a[class*='-logo']")).Click();
            StringAssert.Contains("Stack Overflow", Driver.Title);
        }

        [Test]
        public void ShiftClickExample()
        {
            Driver.Navigate().GoToUrl("https://www.amazon.com/gp/gw/ajax/s.html");
            new Actions(Driver).KeyDown(Keys.Shift).Click(Driver.FindElement(By.CssSelector("a[href*='cart']"))).KeyUp(Keys.Shift).Perform();
            Assert.AreEqual(2, Driver.WindowHandles.Count);
        }

        [Test]
        public void DragAndDropTest()
        {
            Driver.Navigate().GoToUrl("https://jqueryui.com/resources/demos/droppable/default.html");
            var draggable = Driver.FindElement(By.Id("draggable"));
            var droppable = Driver.FindElement(By.Id("droppable"));
            new Actions(Driver).DragAndDrop(draggable, droppable).Build().Perform();
            Assert.IsTrue(Driver.FindElement(By.CssSelector("#droppable > p:nth-child(1)")).Text.Equals("Dropped!"));
        }
    }
}
