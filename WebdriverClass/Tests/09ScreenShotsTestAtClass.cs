﻿using System;
using System.IO;
using NUnit.Framework;

namespace WebdriverClass.Tests
{
    class ScreenShotsTestAtClass : TestBase
    {
        [Test]
        public void ScreenShots()
        {
            Driver.Navigate().GoToUrl("http://www.elvira.hu");

            string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;

            //Maximize browser window

            //Create a screenshot and save the file as screenshot.png

            Assert.IsTrue(File.Exists(baseDirectory + "screenshot.png"));
        }
    }
}
