﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace WebdriverClass.Tests
{
    class WebdriverWaitTest : TestBase
    {
        [Test]
        public void WaitTitle()
        {
            Driver.Navigate().GoToUrl("http://www.google.hu");
            
            IWebElement query = Driver.FindElement(By.Name("q"));
            query.SendKeys("Selenium");
            query.Submit();
            
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(5));
            wait.Until(driver => driver.Title.StartsWith("Selenium"));
            
            Assert.AreEqual("Selenium - Google-keresés", Driver.Title);
        }

        [Test]
        public void WaitKeyboard()
        {
            Driver.Navigate().GoToUrl("http://www.google.hu");

            var keyboardOpenBtnLocator = By.ClassName("MiYK0e");
            Driver.FindElement(keyboardOpenBtnLocator).Click();
            
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(25));
            var keyboardPopupLocator = By.Id("kbd");
            wait.Until(ExpectedConditions.ElementIsVisible(keyboardPopupLocator));

            Driver.FindElement(By.Id("K81")).Click(); //this clicks on q key on keyboard

            var keyboardCloseButtonLocator = By.ClassName("vk-t-btn-o");
            Driver.FindElement(keyboardCloseButtonLocator).Click();

            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(keyboardPopupLocator));

            var searchFormLocator = By.Id("tsf");
            Driver.FindElement(searchFormLocator).Submit();

            Assert.AreEqual("q - Google-keresés", Driver.Title);
        }
    }
}
