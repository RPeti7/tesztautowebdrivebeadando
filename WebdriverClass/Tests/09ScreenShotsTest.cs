﻿using System;
using System.IO;
using NUnit.Framework;
using OpenQA.Selenium;

namespace WebdriverClass.Tests
{
    class ScreenShotsTest : TestBase
    {
        [Test]
        public void ScreenShots()
        {
            Driver.Navigate().GoToUrl("http://www.elvira.hu");
            Driver.Manage().Window.Maximize();

            string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;

            Screenshot ss = ((ITakesScreenshot)Driver).GetScreenshot();

            ss.SaveAsFile(baseDirectory + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss") + ".png", ScreenshotImageFormat.Png);
            ss.SaveAsFile(baseDirectory + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss") + ".jpg", ScreenshotImageFormat.Jpeg);

            ss.SaveAsFile(baseDirectory + "screenshot.png", ScreenshotImageFormat.Png);

            Assert.IsTrue(File.Exists(baseDirectory + "screenshot.png"));
        }
    }
}
