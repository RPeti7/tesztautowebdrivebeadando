﻿using NUnit.Framework;
using WebdriverClass.Pages;
using WebdriverClass.Widgets;

namespace WebdriverClass
{
    class PageObjectTest : TestBase
    {
        [Test]
        public void PageObjectSearchExample()
        {
            SearchWidget searchWidget = SearchPage.Navigate(Driver).GetSearchWidget();

		    searchWidget.SearchForRoute("Budapest", "Szeged", "Kecskemet");

		    searchWidget.SetReductionViaText(SearchWidget.Reductions.TanuloBerlet);
            //in case of Chrome set to english - better would be to set the language
            //searchWidget.setReductionViaText("Students' month pass");

            searchWidget.SetSearchOptionTo(SearchWidget.SearchOptions.PotjegyNelkul);
		
		    ResultWidget resultWidget = searchWidget.ClickTimetableButton().GetResultWidget();

            Assert.Greater(resultWidget.GetNoOfResults(), 0);
        }
    }
}
