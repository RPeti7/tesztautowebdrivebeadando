﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System.Linq;
using NUnit.Framework;
using WebdriverClass.Pages;

namespace WebdriverClass.Widgets
{
    class ResultWidget : BasePage
    {
        public ResultWidget(IWebDriver driver) : base(driver)
        {
        }

        public IWebElement Timetable => Driver.FindElement(By.Id("timetable"));
        public IWebElement ResultTitle => Driver.FindElement(By.ClassName("lrtftop"));

        public int GetNoOfResults()
        {
            return (Timetable.FindElements(By.TagName("tr")).Count() / 2);
        }

        public string ReturnResultTitle()
        {
            return ResultTitle.Text;
        }
    }
}
